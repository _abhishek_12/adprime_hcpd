FROM php:7.2-apache
MAINTAINER vishal "vishal@xorstack.com"


RUN apt-get update &&\
    apt-get install -y \
    git \
    zip \
    curl \
    sudo \
    unzip \
    nano


RUN docker-php-ext-install mysqli pdo pdo_mysql && docker-php-ext-enable pdo_mysql
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

WORKDIR /var/www/html
CMD php artisan serve --host=0.0.0.0 --port=7910
