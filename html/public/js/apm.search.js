var ApmSearch = (function(){


    //holds jQuery Reference -- initialized during init
    var $,
        _$apDataTable,
        _drawingNewData,
        _resp,

        /**
         * duplicates an input row so that users can use additional fields in the where clause
         */
        _duplicate = function(){

            //clone the actual form, append to container
            $('#ap-search-fieldgroup').clone().removeAttr('id').find('select').trigger('reset').end()
                .find('input').val('').end().find('*').removeAttr('id').removeClass('has-success').removeClass('has-danger')
                .end().appendTo('#ap-search-dupes');

            //hide add button
            $('.ap-dupe-field').hide().last().show();

            //show remove button

            //display tooltips
            _toggleTooltips();

        },

        /**
         * removes the field group that belongs to the button that was pressed
         *
         * @param $this
         * @private
         */
        _remove = function($this){

            //remove the field group that the current object belongs to
            $this.closest('.ap-fieldgroup').remove();

            //show add button
            $('.ap-dupe-field').hide().last().show();

            //remove remnant tooltips
            $('div.tooltip').remove();

            //display tooltips
            _toggleTooltips();
        },


        /**
         * clears the form and removes all duplicate fields
         * @private
         */
        _clear = function(){

            //remove all dupes
            $('#ap-search-dupes').empty();


            //reset remaining form items
            $('#ap-search-form').trigger('reset');

            //show add button
            $('.ap-dupe-field').hide().last().show();

            //remove remnant tooltips
            $('div.tooltip').remove();

            //remove validation
            $('.has-danger').removeClass('has-danger');
            $('.has-success').removeClass('has-success');

            //display tooltips
            _toggleTooltips();

        },

        /**
         * does light validation on form, ensures that the field name is not null and value is not empty
         * @private
         */
        _validateForm = function(){

            $('.has-danger').removeClass('has-danger');

            var $fields = $('[name^=field]'),
                $values = $('[name^=value]'),
                isValid = true;

            for(var i=0; i<$fields.length; i++){
                if($($fields[i]).val() == null){
                    isValid = false;
                    $($fields[i]).closest('div.col').addClass('has-danger');
                }else{
                    $($fields[i]).closest('div.col').addClass('has-success');
                }
            }

            for(var i=0; i<$values.length; i++){
                if( $($values[i]).val().trim().length == 0){
                    isValid = false;
                    $($values[i]).closest('div.col').addClass('has-danger');
                }else{
                    $($values[i]).closest('div.col').addClass('has-success');
                }
            }

            return isValid;
        },

        /**
         * collects search params and triggers a search
         * @private
         */
        _search = function(){


            if(!_validateForm())
                return false;

            var params = $('form#ap-search-form').serializeArray();

            $('#results').hide();
            $('#ap-back-end').show();

            //initialize loading spinner
            //querying DB

            $.post('/search', params, function(resp){

                $('#ap-back-end').hide();
                $('#ap-building').show();

                //passing parameters
                (function(window, hcps) {
                    setTimeout(function () {
                        _drawingNewData = false;
                        _$apDataTable.clear().draw();
                        _$apDataTable.rows.add(hcps);
                        _drawingNewData = true;
                        _$apDataTable.columns.adjust().draw();
                        $('#results').show();
                    },100);
                })(window, resp.hcps);




            }).fail(function(data){
                var errors = data.responseJSON;
                console.log('error',errors,  arguments);

                //stop spinner
                $('.ap-loading-box').hide();
                //error has occured


            });

        },

        _toggleTooltips = function(){
            $('[data-toggle="tooltip"]').tooltip()
        },


        _init = function(jQuery){
            $ = jQuery;


            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
                },
                timeout: 300000
            });

            ApmSearch.showTooltips();

            $(document).on('click', 'a.ap-dupe-field', function(e){
                e.preventDefault();
                ApmSearch.dupeFields();
            });

            $(document).on('click', 'span.ap-rem-field', function(e){
                e.preventDefault();
                ApmSearch.removeField($(this));
            });

            $(document).on('change', 'select, input', function(){
                console.log('changed');
                _validateForm();
            });

            $('#ap-clear-form').click(function(e){
                e.preventDefault();
                ApmSearch.clearForm();
            });

            $('#ap-search-submit').click(function(e){
                e.preventDefault();
                ApmSearch.search(jQuery);
            });


            /**
             * Init Data Table
             */
            _$apDataTable = $('#ap-results').DataTable({
                data:[],
                "deferRender": true,
                "columns": [
                    { "data" : "npi"},
                    { "data" : "fname"},
                    { "data" : "lname"},
                    { "data" : "city"},
                    { "data" : "state"},
                    { "data" : "zip"},
                    { "data" : "primary_taxonomy"}
                ]
            });


            $('#ap-results').on('draw.dt', function(){
                if(_drawingNewData){
                    _drawingNewData = false;
                    $('.ap-loading-box').hide();
                }
            })

        };

    return{
        dupeFields:_duplicate,
        removeField:_remove,
        clearForm:_clear,
        search:_search,
        showTooltips:_toggleTooltips,
        init: _init
    };

})();


$(function(){
    ApmSearch.init($);

});

