@extends('layouts.main')

@section('title')
    Search @endsection


@section('content')
    <div class="row ap-head">
        <div class="col">
            <h1>HCP DB Search Tool</h1>
            <p>
                Fill out the form below to perform a basic query against Adprime's Healthcare Provider Database<br>
                <a href="\taxonomy"> View HCP Taxonomies </a>
            </p>
        </div>
    </div>

    <form id="ap-search-form">
        {{ csrf_field() }}

        <!-- start cloneable row -->
        <div id="ap-search-fieldgroup" class="ap-fieldgroup">
            <div class="row form-group align-items-center">

                <div class="col">
                    <select name="field[]" id="ap-field-name" class="form-control">
                        <option value="" disabled selected>Field Name</option>
                        <option value="npi">NPI</option>
                        <option value="first_name">First Name</option>
                        <option value="last_name">Last Name</option>
                        <option value="specialty">Specialty</option>
                        <option value="taxonomy">Taxonomy Code</option>
                        <option value="state">State</option>
                        <option value="city">City</option>
                        <option value="zip">Zip</option>
                    </select>
                </div>

                <div class="col">
                    <select name="comparator[]" id="ap-compare-type" class="form-control">
                        <option value="eq" selected > Equals ( = )</option>
                        <option value="like" > Similar To ( like ) </option>
                    </select>
                </div>
                <div class="col">
                    <input type="text" name="value[]" class="form-control" placeholder="Value">
                </div>

                <div class="col-1 text-nowrap">
                    <a href="#" class="btn btn-success btn-sm ap-dupe-field" data-toggle="tooltip" data-placement="top" title="Add Field">
                        <i class="fa fa-plus-circle"></i>
                    </a>

                    <span class="badge badge-danger ap-rem-field" data-toggle="tooltip" data-placement="right" title="Remove">
                        <i class="fa fa-times-circle"></i>
                    </span>
                </div>

            </div>


        </div>
        <div id="ap-search-dupes">
            <!-- duplicate fields go here -->
        </div>
        <!-- end cloneable row -->
        <div class="ap-search-submit">
            <div class="row form-group justify-content-end">
                <div class="col-2">
                    <button class="btn btn-secondary float-right" id="ap-clear-form">Clear</button>
                </div>
                <div class="col-1">
                    <button class="btn btn-primary" id="ap-search-submit">Search</button>
                </div>
            </div>
        </div>

    </form>

    <div class="col-12" id="results" style="display:none;">
        <table id="ap-results" class="display" cellspacing =0 width="100%">
            <thead>
                <th>NPI #</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>City</th>
                <th>State</th>
                <th>Zip</th>
                <th>Primary Taxonomy</th>
            </thead>
            <tfoot>
                <th>NPI #</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>City</th>
                <th>State</th>
                <th>Zip</th>
                <th>Primary Taxonomy</th>
            </tfoot>
            <tbody>

            </tbody>
        </table>
    </div>

    <div class="row justify-content-md-center">
        <div class="col-3">
            <div id="ap-back-end" class="ap-loading-box">
                <span class="ap-loading-text">Querying DB</span>
                <i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i>
                <span class="sr-only">Querying DB...</span>
            </div>
            <div id="ap-building" class="ap-loading-box">
                <span class="ap-loading-text">Building Table</span>
                <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                <span class="sr-only">Loading...</span>
            </div>
        </div>
    </div>

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.css">
@endpush


@push('scripts')
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.js"></script>
<script src="/js/apm.search.js"></script>
@endpush
