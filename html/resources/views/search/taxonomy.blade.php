@extends('layouts.main')

@section('title')
    Taxonomy
@endsection

@section('content')

    <div class="row ap-head">
        <div class="col">
            <h1>HCP Taxonomies</h1>
            <p>
                Taxonomy codes and explanations are listed below
            </p>
        </div>
    </div>

    <div class="row ap-tax-container">
        <div class="col-12">

            <table id="ap-taxonomy" class="display" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th width="10%">Code</th>
                        <th width="15%">Grouping</th>
                        <th width="15%">Classification</th>
                        <th width="13%">Specialization</th>
                        <th width="7%">Pop.</th>
                        <th width="35%" >Definition</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Code</th>
                        <th>Grouping</th>
                        <th>Classification</th>
                        <th>Specialization</th>
                        <th>Population</th>
                        <th>Definition</th>
                    </tr>
                </tfoot>
                <tbody>
                    @foreach($data as $row)
                        @if(strlen($row->code)>0)
                            <tr>
                                <td>{{ $row->code }}</td>
                                <td>{{ $row->grouping }}</td>
                                <td>{{ $row->classification }}</td>
                                <td>{{ $row->specialization }}</td>
                                <td>
                                    @if(isset($pop[$row->code]))
                                        {{ number_format($pop[$row->code]) }}
                                    @else
                                        0
                                    @endif
                                </td>
                                <td>{{ $row->definition }}</td>
                            </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>



@endsection


@push('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.css">
@endpush

@push('scripts')
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready(function() {
            $('#ap-taxonomy').DataTable(

            );
        });
    </script>
@endpush