<?php
/**
 * Created by: r.chachura
 * Date: 4/24/17
 */

namespace App\Http\Controllers;

use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SearchController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    private $dbFields =[
        'npi' => ['nppes.npi'],
        'last_name'=> ['provider_last_name'],
        'first_name' => ['provider_first_name'],
        'city'  => [/*'provider_business_mailing_address_city_name',*/ 'provider_business_practice_location_address_city_name'],
        'state'  => [/*'provider_business_mailing_address_state_name',*/ 'provider_business_practice_location_address_state_name'],
        'zip'  => [/*'provider_business_mailing_address_postal_code',*/ 'provider_business_practice_location_address_postal_code']
    ];


    /**
     * Displays the  search form
     */
    public function index(){
        $data = [
            'name' => "Laravel Name"
        ];
        return view('search.index', $data);
    }

    /**
     * Does the actual searching - returns JSON
     * @param Request $request
     * @return redirect to home or json
     */
    public function search(Request $request){


//        if(!$request->ajax()){
//            return redirect()->route('home');
//        }

        //get request values
        $values = $request->value;
        $comparators = $request->comparator;
        $fields = $request->field;

        $clauses = [];
        //build where clause
        for($i=0; $i<count($fields); $i++){
            if($fields[$i] == 'specialty'){

                //perform sub query for taxonomies that match
                $spec = '%'.$values[$i].'%';

                $taxonomies = DB::select("select * from taxonomies 
                WHERE grouping like :spec or specialization like :spec or classification like :spec or definition like :spec ;",
                [':spec'=>$spec]);

                $codes = [];
                //find all NPIs that support this Taxonomy
                foreach($taxonomies as $tx){
                    $codes[] = $tx->code;
                }
                $codeString = implode("','", $codes);
                $clauses[] = "nppes.npi in (select npi from npi_taxonomies where healthcare_provider_taxonomy_code in ('$codeString'))";

            }else if($fields[$i] == 'taxonomy'){
                //perform taxonomy npi_taxonomy query
                $tax = $values[$i];
                $clause = "nppes.npi in (select npi from npi_taxonomies where healthcare_provider_taxonomy_code ";
                if($comparators[$i] == 'eq')
                    $clause .= " = '$tax')";
                else
                    $clause .= " like '%$tax%')";

                $clauses[] = $clause;
                //append npis to where clasue

            }else{
                $subClauses = [];
                //find the fields in the array
                foreach($this->dbFields[$fields[$i]] as $f ){

                    if($comparators[$i] == 'eq'){
                        $clause = sprintf(" $f = '%s' ", $values[$i]);
                    }else{
                        $clause = sprintf(" $f like '%s' ", '%'.$values[$i].'%');
                    }
                    $subClauses[]=$clause;
                }

                $subClause = " ( " . implode(" OR ", $subClauses) . ") ";
                $clauses[] = $subClause;
                //construct where clasue
            }

        }

        //construct basic query
        $query = "select nppes.npi, provider_first_name as fname, provider_last_name as lname, provider_business_practice_location_address_city_name as city, provider_business_practice_location_address_state_name as state,provider_business_practice_location_address_postal_code as zip, healthcare_provider_taxonomy_code as primary_taxonomy  from nppes 
          INNER JOIN npi_taxonomies t on t.npi = nppes.npi
          Where  healthcare_provider_primary_taxonomy_switch = 'Y' AND entity_type_code = 1 AND ";

        //append where clauses
        $query .= implode(" AND ", $clauses) .';';
        $hcps = DB::select($query);

        
//        return response()->json(['v'=>$values, 'c'=>$comparators, 'f'=>$fields, 'hcps'=>$hcps, 'query'=>$query, 'where'=>$clauses]);
        return response()->json(['hcps'=>$hcps]);




    }



    /**
     * @param Request $request
     * @return View
     */
    public function taxonomy(Request $request){

        $taxonomies = DB::select("select * from taxonomies");

        $pubPath = public_path();
        $today_date = date("Y-m-d");
        $filename = "taxpop_".$today_date.".json";
        $fileloc = $pubPath."/data/".$filename;
        if(is_file($fileloc)){
            $pops = json_decode(file_get_contents($fileloc),true);
        }
        else{
            $taxPopulations = DB::select("select healthcare_provider_taxonomy_code as code, count(npi) as pop from npi_taxonomies group by healthcare_provider_taxonomy_code;");
            $pops = [];
            foreach ($taxPopulations as $popRow) {
                if (strlen($popRow->code) > 0) {
                    $pops[$popRow->code] = $popRow->pop;
                }
            }

            file_put_contents($fileloc, json_encode($pops));
            session(['taxPop'=>$pops]);
        }

        return view('search.taxonomy', ['data' => $taxonomies, 'pop'=>$pops]);
    }



}